# Movie-ElasticSearch-RHLC
### 简介
使用 SpringBoot2.1 + ElasticSearch6.4.3 + RestHighLevelClient（官方推荐） 实现了一个简单的电影搜索网站Demo<br>
顺便将 RestHighLevelClient API做了封装，极大简化 java与es的常见交互。


### 本地部署
* 启动 ElasticSearch 6.X+
* 修改 application.properties 中的  spring.data.elasticsearch.cluster-name 和 spring.data.elasticsearch.cluster-node 参数
* 启动 SpringBoot 项目
* 访问 <localhost:88> 开始搜索

### 为何要写这个 工具

在学习 ElasticSearch 的过程中，发现 ElasticSearch 的 Java 客户端选择很多，这种现象对于选择困难症患者来说不是非常友好<br>

本来 spring-data-elasticsearch 应该是首要选择，深入使用后发现 spring-data-elasticsearch 基于 TransportClient，ElasticSearch 
官方准备废弃该客户端 转推 RestHighLevelClient 客户端。<br>

RestHighLevelClient API复杂并且资料不多， 此工具以ElasticSearch官方推荐的RestHighLevelClient进行封装，能够极大简化java与ElasticSearch的常见交互，它让ElasticSearch的高级的功能轻松被使用。
<br>

### 参考资料
* [SpringBoot 官方文档 ElasticSearch 部分](https://docs.spring.io/spring-boot/docs/2.1.3.RELEASE/reference/htmlsingle/#boot-features-elasticsearch)
* [个人整理的 ElasticSearch 学习思维导图](http://naotu.baidu.com/file/3b7f1dec1a487abf6ffe51f1a950744b?token=e08546f6ca1fa320)
