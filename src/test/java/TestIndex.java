import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ak.es.index.ElasticsearchIndex;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestIndex{
    @Autowired
    ElasticsearchIndex<MainTest> elasticsearchIndex;
    @Test
    public void testIndex() throws Exception {
        if(!elasticsearchIndex.exists(MainTest.class)){
            elasticsearchIndex.dropIndex(MainTest.class);
            elasticsearchIndex.createIndex(MainTest.class);
        }
    }
}
