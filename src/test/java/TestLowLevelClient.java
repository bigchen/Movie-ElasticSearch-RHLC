import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ak.es.repository.ElasticsearchTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestLowLevelClient {

    @SuppressWarnings("rawtypes")
	@Autowired
    ElasticsearchTemplate elasticsearchTemplate;

    @Test
    public void testLow() throws Exception {
        Request request = new Request("GET","/esdemo");
        request.setEntity(new NStringEntity(
                "{\"query\":{\"match_all\":{\"boost\":1.0}}}",
                ContentType.APPLICATION_JSON));
        Response response = elasticsearchTemplate.request(request);
        String responseBody = EntityUtils.toString(response.getEntity());
        System.out.println(responseBody);
    }
}
