import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ak.es.entity.HighLight;
import com.ak.es.entity.Page;
import com.ak.es.entity.PageSortHighLight;
import com.ak.es.entity.Sort;
import com.ak.es.repository.ElasticsearchTemplate;
import com.ak.es.utils.JsonUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestCRUD {
    @Autowired
    ElasticsearchTemplate<MainTest,String> elasticsearchTemplate;

    @Test
    public void testsave() throws Exception {
        MainTest main1 = new MainTest();
        main1.setProposal_no("main11");
        main1.setAppli_code("123");
        main1.setAppli_name("spring");
        main1.setRisk_code("0501");
        main1.setSum_premium(100);
        elasticsearchTemplate.save(main1);
    }

    @Test
    public void testsave2() throws Exception {
        MainTest main1 = new MainTest();
        main1.setProposal_no("main1123123123");
        main1.setAppli_code("123");
        main1.setAppli_name("2");
        main1.setRisk_code("0501");
        main1.setSum_premium(100);
        elasticsearchTemplate.save(main1);
    }


    @Test
    public void testsavelist() throws Exception {
        List<MainTest> list = new ArrayList<>();
        MainTest main1 = new MainTest();
        main1.setProposal_no("main1");
        main1.setAppli_name("456");
        main1.setRisk_code("0101");
        main1.setSum_premium(1);
        MainTest main2 = new MainTest();
        main2.setProposal_no("main2");
        main2.setAppli_name("456");
        main2.setSum_premium(2);
        main2.setRisk_code("0102");
        MainTest main3 = new MainTest();
        main3.setProposal_no("main3");
        main3.setRisk_code("0103");
        main3.setSum_premium(3);
        main3.setAppli_name("456");
        MainTest main4 = new MainTest();
        main4.setProposal_no("33333333");
        main4.setRisk_code("0103");
        main4.setSum_premium(4);
        main4.setAppli_name("123");
        MainTest main5 = new MainTest();
        main5.setProposal_no("11111111");
        main5.setRisk_code("0103");
        main5.setAppli_name("123");
        main5.setSum_premium(5);
        MainTest main6 = new MainTest();
        main6.setProposal_no("22222222");
        main6.setRisk_code("0103");
        main6.setAppli_name("123");
        main6.setSum_premium(6);
        list.add(main1);
        list.add(main2);
        list.add(main3);
        list.add(main4);
        list.add(main5);
        list.add(main6);
        elasticsearchTemplate.save(list);
    }


    @Test
    public void testUpdate() throws Exception {
        MainTest main1 = new MainTest();
        main1.setProposal_no("main1");
        main1.setInsured_code("123");
        elasticsearchTemplate.update(main1);
    }


    @Test
    public void testCoverUpdate()throws Exception {
        MainTest main1 = new MainTest();
        main1.setProposal_no("main1");
        main1.setInsured_code("123");
        elasticsearchTemplate.updateCover(main1);
    }


    @Test
    public void testDelete()throws Exception {
        MainTest main1 = new MainTest();
        main1.setProposal_no("main1");
        main1.setInsured_code("123");
        elasticsearchTemplate.delete(main1);

        elasticsearchTemplate.deleteById("main1",MainTest.class);

    }

    @Test
    public void testExists()throws Exception {
        MainTest main1 = new MainTest();
        main1.setProposal_no("main1");
        main1.setInsured_code("123");
        boolean exists = elasticsearchTemplate.exists("main1",MainTest.class);
        System.out.println(exists);
    }


    @Test
    public void testOri()throws Exception {
        SearchRequest searchRequest = new SearchRequest(new String[]{"index"});
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(new MatchAllQueryBuilder());
        searchSourceBuilder.from(0);
        searchSourceBuilder.size(10);
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = elasticsearchTemplate.search(searchRequest);

        SearchHits hits = searchResponse.getHits();
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit hit : searchHits) {
            MainTest t = JsonUtils.string2Obj(hit.getSourceAsString(), MainTest.class);
            System.out.println(t);
        }
    }



    @Test
    public void testSearch()throws Exception {
        List<MainTest> main2List = elasticsearchTemplate.search(new MatchAllQueryBuilder(),MainTest.class);
        main2List.forEach(main2 -> System.out.println(main2));
    }



    @Test
    public void ttttt(){
        SearchRequest searchRequest = new SearchRequest("index");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(new MatchAllQueryBuilder());
//        searchSourceBuilder.from(0);
//        searchSourceBuilder.size(Constant.DEFALT_PAGE_SIZE);
        searchRequest.source(searchSourceBuilder);
    }

    @Test
    public void testsaveHighlight() throws Exception {
        MainTest main1 = new MainTest();
        main1.setProposal_no("main1123123123");
        main1.setAppli_code("123");
        main1.setAppli_name("一二三四五唉收到弄得你阿斯达岁的阿斯蒂芬斯蒂芬我单位代缴我佛非我方是的佛挡杀佛第三方东方闪电凡事都红is都if觉得搜房水电费啥都if结算单佛第四届发送到");
        main1.setRisk_code("0501");
        main1.setSum_premium(100);
        elasticsearchTemplate.save(main1);
    }

    @Test
    public void testSearch2()throws Exception {
        int currentPage = 1;
        int pageSize = 10;
        //分页
        PageSortHighLight psh = new PageSortHighLight(currentPage,pageSize);
        //排序
        String sorter = "proposal_no.keyword";
        Sort.Order order = new Sort.Order(SortOrder.ASC,sorter);
        psh.setSort(new Sort(order));
        //定制高亮，如果定制了高亮，返回结果会自动替换字段值为高亮内容
        psh.setHighLight(new HighLight().field("appli_name"));
        //可以单独定义高亮的格式
        Page<MainTest> pageList = new Page<MainTest>();
        pageList = elasticsearchTemplate.search(pageList, QueryBuilders.matchQuery("appli_name","我"), psh, MainTest.class);
        pageList.getList().forEach(main2 -> System.out.println(main2));
    }

    @Test
    public void testCount()throws Exception {
        long count = elasticsearchTemplate.count(new MatchAllQueryBuilder(),MainTest.class);
        System.out.println(count);
    }


    @Test
    public void testScroll()throws Exception {
        //默认scroll镜像保留2小时
        List<MainTest> main2List = elasticsearchTemplate.scroll(new MatchAllQueryBuilder(),MainTest.class);
        main2List.forEach(main2 -> System.out.println(main2));

        //指定scroll镜像保留5小时
        //List<MainTest> main2List = elasticsearchTemplate.scroll(new MatchAllQueryBuilder(),MainTest.class,5);
    }


    @Test
    public void testCompletionSuggest()throws Exception {
        List<String> list = elasticsearchTemplate.completionSuggest("appli_name", "1", MainTest.class);
        list.forEach(main2 -> System.out.println(main2));
    }


    @Test
    public void testSearchByID()throws Exception {
        MainTest main2 = elasticsearchTemplate.getById("main2", MainTest.class);
        System.out.println(main2);
    }

    @Test
    public void testMGET()throws Exception {
        String[] list = {"main2","main3"};
        List<MainTest> listResult = elasticsearchTemplate.mgetById(list, MainTest.class);
        listResult.forEach(main -> System.out.println(main));
    }

    @Test
    public void testQueryBuilder() throws Exception {
        QueryBuilder queryBuilder = QueryBuilders.boolQuery().must(QueryBuilders.termQuery("appli_name.keyword","456"))
                .filter(QueryBuilders.matchPhraseQuery("risk_code","0101"));
        List<MainTest> list = elasticsearchTemplate.search(queryBuilder,MainTest.class);
        list.forEach(main2 -> System.out.println(main2));
    }
}
