package com.ak.movie.repository;

import com.ak.es.entity.Page;
import com.ak.movie.entity.Movie;
import com.ak.movie.entity.QueryDTO;

public interface IMovieRepository {

	boolean save(Movie movie) throws Exception;

	Page<Movie> query(String queryString, int pageNo, int size) throws Exception;

	Page<Movie> query(QueryDTO queryDTO, int pageNo, int size) throws Exception;

	Movie get(String id) throws Exception;
}
