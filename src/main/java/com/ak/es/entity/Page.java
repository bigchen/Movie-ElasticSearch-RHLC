package com.ak.es.entity;

import java.util.List;

import lombok.Data;

/**
 * @program: AK-ES
 * @description: 分页对象封装
 * @author: AK
 * @create: 2019-01-21 17:06
 **/
@Data
public class Page<T> {

	private int pageNo;

	private int size;

	List<T> list;

	private int totalPages = 0;

	private long totalElements = 0;

	public Page() {

	}

	public Page(int pageNo, int size) {
		this.pageNo = pageNo;
		this.size = size;
	}

	public boolean hasNext() {
		if (pageNo * size >= totalElements) {
			return false;
		} else {
			return true;
		}
	}

	public boolean hasPrevious() {
		if (pageNo > 1) {
			return true;
		} else {
			return false;
		}
	}
}
