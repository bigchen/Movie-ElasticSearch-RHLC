package com.ak.es.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @program: AK-ES
 * @description: 分页+高亮对象封装
 * @author: AK
 * @create: 2019-01-21 17:09
 **/
@Getter
@Setter
public class PageSortHighLight {
	private int currentPage;
	private int pageSize;
	private Sort sort = new Sort();
	private HighLight highLight = new HighLight();

	public PageSortHighLight(int currentPage, int pageSize) {
		this.currentPage = currentPage;
		this.pageSize = pageSize;
	}

	public PageSortHighLight(int currentPage, int pageSize, Sort sort) {
		this.currentPage = currentPage;
		this.pageSize = pageSize;
		this.sort = sort;
	}
}
