package com.ak.es.entity;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @program: AK-ES
 * @description: 高亮对象封装
 * @author: AK
 * @create: 2019-01-23 11:13
 **/
public class HighLight {
	@Getter
	@Setter
	private String preTag = "";
	@Getter
	@Setter
	private String postTag = "";

	private List<String> highLightList = null;

	public HighLight() {
		highLightList = new ArrayList<>();
	}

	public HighLight field(String fieldValue) {
		highLightList.add(fieldValue);
		return this;
	}

	public List<String> getHighLightList() {
		return highLightList;
	}
}