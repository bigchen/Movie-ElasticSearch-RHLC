package com.ak.es.entity;

import lombok.Data;

/**
 * @program: AK-ES
 * @description: 下钻聚合分析返回对象
 * @author: AK
 * @create: 2019-02-19 16:06
 **/
@Data
public class Down {
	String level_1_key;
	String level_2_key;
	Object value;
}
