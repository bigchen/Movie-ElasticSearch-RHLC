package com.ak.es.config;

import javax.annotation.Nonnull;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @program: esclientrhl
 * @description: todo 自动注册repostory
 * @author: AK
 * @create: 2019-04-16 15:24
 **/
public class ESToolsRegistrar implements BeanFactoryAware, ApplicationContextAware, ImportBeanDefinitionRegistrar,
		ResourceLoaderAware, EnvironmentAware {
	private @SuppressWarnings("null") @Nonnull ResourceLoader resourceLoader;
	private @SuppressWarnings("null") @Nonnull Environment environment;
	@SuppressWarnings("unused")
	private ApplicationContext applicationContext;
	@SuppressWarnings("unused")
	private BeanFactory beanFactory;

	@Override
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

	@Override
	public void registerBeanDefinitions(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry registry) {
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}

}
