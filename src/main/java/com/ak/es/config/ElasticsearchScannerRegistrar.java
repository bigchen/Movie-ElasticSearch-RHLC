package com.ak.es.config;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @program: AK-ES
 * @description: ${description}
 * @author: AK
 * @create: 2019-01-24 17:25
 **/
public class ElasticsearchScannerRegistrar implements ImportBeanDefinitionRegistrar, ResourceLoaderAware {

	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
	}

	@Override
	public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
	}
}
