package com.ak.es.enums;

/**
 * @program: AK-ES
 * @description: es字段数据结构
 * @author: AK
 * @create: 2019-01-25 16:58
 **/
public enum DataType {
	keyword_type, text_type, byte_type, short_type, integer_type, long_type, float_type, double_type, boolean_type,
	date_type;
}
