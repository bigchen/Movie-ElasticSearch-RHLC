package com.ak.es.enums;

/**
 * @program: AK-ES
 * @description: 聚合metric类型枚举
 * @author: AK
 * @create: 2019-02-01 13:44
 **/
public enum AggsType {
	sum, min, max, count, avg
}
