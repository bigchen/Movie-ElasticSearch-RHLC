<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width">
    <title>movie-elasticsearch</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/common.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery_lazyload/1.9.7/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="/js/common.js"></script>
    <style>
        em{
            color: orangered;
        }
        form{
            max-width: 280px;
            max-width: calc(100% - 100px);
        }
    </style>
</head>
<body class="container">
<header>
    <h1><a href="/">Movie ElasticSearch</a></h1>
    <form action="/s" class="input-group">
        <input name="wd" class="form-control" value='${wd}'>
        <div class="input-group-btn">
            <button type="submit" class="btn btn-primary">搜索</button>
        </div>
    </form>
</header>
    <#if page??>
    <section>
        <p>共找到相关结果${page.totalElements}个</p>
    </section>
    <section>
        <#list page.list as movie>
            <div>
                <p>
                    <a href="/detail/${movie.id}">
                        ${movie.name!""}<#if movie.translatedName??><#list movie.translatedName as tn>/${tn}</#list></#if>
                    </a>
                </p>
                <p><img
                        src = "https://img3.doubanio.com/f/movie/03d3c900d2a79a15dc1295154d5293a2d5ebd792/pics/movie/tv_default_large.png"
                    <#if movie.coverUrl??>data-original="${movie.coverUrl}"</#if>
                        class="img-thumbnail lazy"
                        onerror="notFound()"
                >
                </p>
                <p>${movie.description!"暂无简介"}</p>
            </div>
        </#list>
    </section>
    <nav aria-label="...">
        <ul class="pager">
            <li class="previous<#if !page.hasPrevious()> disabled</#if>">
                <a href=<#if page.hasPrevious()>"/s?wd=${wd}&pn=${page.pageNo - 1}"<#else>#</#if>><span aria-hidden="true">&larr;</span> 上一页</a>
            </li>
            <li class="next<#if !page.hasNext()> disabled</#if>" >
                <a href=<#if page.hasNext()>"/s?wd=${wd}&pn=${page.pageNo + 1}"<#else>#</#if>>下一页 <span aria-hidden="true">&rarr;</span></a>
            </li>
        </ul>
    </nav>
    </#if>
<script>
    $("img.lazy").lazyload();
</script>
</body>
</html>